from . import replace_rigify_ui, rig_types_ui, bone_selection_pie_menu

modules = [
    replace_rigify_ui,
    rig_types_ui,
    bone_selection_pie_menu
]