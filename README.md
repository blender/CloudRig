# This project now lives on [projects.blender.org](https://projects.blender.org/Mets/CloudRig).

You can still download old versions of CloudRig here up to Blender 3.6, but you can find those at the [new place](https://projects.blender.org/Mets/CloudRig/releases) as well.

Old versions of CloudRig will still lead to this repo when opening the manual or reporting a bug.
I would ask that you go ahead and report bugs at the above link instead, and not here.

# What is CloudRig?
CloudRig was a collection of customizable rig building blocks to extend Blender's Rigify system.
As of Blender 4.1, CloudRig is a standalone add-on rather than a Rigify extension.

You can support the development of CloudRig by subscribing to the [Blender Studio](https://studio.blender.org/)!
